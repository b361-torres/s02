from django.urls import path

from . import views

app_name = 'grocerylist'
urlpatterns = [
    path('', views.index, name='index'),
    path('groceryitem/<int:groceryitem_id>/', views.groceryitem, name='viewgroceryitem'),
    path('event/<int:event_id>/', views.event, name='viewevent'),
    path('register/', views.register, name='register'),
    path('change_password/', views.change_password, name='change_password'),
    path('login/', views.login_view, name='login'),
    path('logout/', views.logout_view, name='logout'),
    path('add_event/', views.add_event, name='add_event'),
    path('add_grocery/', views.add_grocery, name='add_grocery'),
]