from django.contrib import admin
from .models import GroceryItem, Event

# Register your models here.

admin.site.register(GroceryItem)
admin.site.register(Event)