from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

class RegisterForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password']

    def clean_username(self):
        username = self.cleaned_data['username']
        if User.objects.filter(username=username).exists():
            raise ValidationError("This username is already taken.")
        return username

class LoginForm(forms.Form):
	username = forms.CharField(label='Username', max_length=20)
	password = forms.CharField(label='Password', max_length=20)

class AddEventForm(forms.Form):
    event_name = forms.CharField(label='Event Name', max_length=50)
    description = forms.CharField(label='Description', max_length=200)
    status = forms.CharField(label='Status', max_length=50, required=False)

class AddGroceryForm(forms.Form):
	item_name = forms.CharField(label='Item Name', max_length=50)
	category = forms.CharField(label='Category', max_length=200)