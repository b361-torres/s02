from django.shortcuts import render, redirect, get_object_or_404
# from django.http import HttpResponse
# from django.template import loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from django.utils import timezone

# local imports
from .models import GroceryItem, Event
from .forms import LoginForm, RegisterForm, AddEventForm, AddGroceryForm

# Create your views here.
def index(request):
    groceryitem_list = GroceryItem.objects.all()
    event_list = Event.objects.all()  # Fetch all events from the database
    context = {'groceryitem_list': groceryitem_list, 'event_list': event_list}
    return render(request, "groceryitem/index.html", context)


def groceryitem(request, groceryitem_id):

    # retrieves the item objects using 
    groceryitem = get_object_or_404(GroceryItem, pk=groceryitem_id)

    # response = "You are viewing the details of %s"
    return render(request, "groceryitem/groceryitem.html", model_to_dict(groceryitem))

def event(request, event_id):

    # retrieves the item objects using 
    event = get_object_or_404(Event, pk=event_id)

    # response = "You are viewing the details of %s"
    return render(request, "groceryitem/event.html", model_to_dict(event))


def register(request):
    context = {}

    if request.method == 'POST':
        form = RegisterForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data['username']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']

            # Checks the database if a user with the same username already exists
            if User.objects.filter(username=username).exists():
                context['error'] = True
            else:
                user = User.objects.create_user(username=username, first_name=first_name, last_name=last_name, email=email, password=password)
                user.is_staff = False
                user.is_active = True
                user.save()
                return redirect('grocerylist:login')
    else:
        form = RegisterForm()

    context['form'] = form  # Pass the form to the template for error display

    return render(request, "groceryitem/register.html", context)

def change_password(request):

    is_user_authenticated = False
    user = authenticate(username ="johndoe", password = "john1234")
    print(user)
    if user is not None:
        authenticated_user = User.objects.get(username='johndoe')
        authenticated_user.set_password("johndoe1")
        authenticated_user.save()
        is_user_authenticated = True

    context = {
        "is_user_authenticated" : is_user_authenticated
        }
    
    return render(request, "groceryitem/change_password.html", context)

def add_event(request):
    context = {}

    if request.method == 'POST':
        form = AddEventForm(request.POST)

        if form.is_valid():
            event_name = form.cleaned_data['event_name']
            description = form.cleaned_data['description']

            # Check the database if an event with the same name already exists
            duplicates = Event.objects.filter(event_name=event_name)

            # If no duplicates found, create a new Event
            if not duplicates:
                Event.objects.create(
                    event_name=event_name,
                    description=description,
                    date_created=timezone.now(),
                    user=request.user  # Set the user directly, no need for user_id=request.user.id
                )
                return redirect('grocerylist:index')  # Redirect to a success page or event list
            else:
                context['error'] = True  # Set the error flag in the context
        else:
            context['form'] = form  # Pass the form with errors back to the template

    return render(request, "groceryitem/add_event.html", context)

def add_grocery(request):
    context = {}

    if request.method == 'POST':
        form = AddGroceryForm(request.POST)

        if form.is_valid():
            item_name = form.cleaned_data['item_name']
            category = form.cleaned_data['category']

            # Check the database if a grocery item with the same name already exists
            duplicates = GroceryItem.objects.filter(item_name=item_name)

            # If no duplicates found, create a new GroceryItem
            if not duplicates:
                GroceryItem.objects.create(
                    item_name=item_name,
                    category=category,
                    date_created=timezone.now(),
                    user=request.user  # Set the user directly, no need for user_id=request.user.id
                )
            else:
                context = {
                    "error": True
                }
        else:
            # Handle the case when the form is not valid (e.g., display form errors)
            context['form'] = form

    return render(request, "groceryitem/add_grocery.html", context)


def login_view(request):
    # username = "johndoe"
    # password = "johndoe1"
    # user = authenticate(username=username, password=password)

    context = {}

    if request.method == 'POST':

        form = LoginForm(request.POST)
            # Returns a blank Login form
        if form.is_valid() == False:
            form = LoginForm()
        else:
            # Retrieves the information from the form
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            context = {
                "username": username,
                "password": password
            }
        if user is not None:
            # Saves the user's ID in the session using Django's session framework
            login(request,user)
            return redirect("grocerylist:index")
        else:
            # Provides context with error to conditionally render the error message
            context = {
                "error": True
            }


    return render(request, "groceryitem/login.html", context)

def logout_view(request):
    logout(request)
    return redirect("grocerylist:index")
